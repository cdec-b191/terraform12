
provider "aws" {
    region = "eu-west-3"
}

# Launch Aws instance

resource "aws_instance" "my_instance" {
    ami = var.image_id
    instance_type = var.machine_type   
    key_name = "paris-key"
    vpc_security_group_ids = ["sg-044cee61e42702ab4"]
    tags = {
        Name = "my-instance"
        env = "dev"
    } 
}

# create security group

resource "aws_security_group" "my_sg" {
    name = "my-security-group"
    description = "allow-ssh and http"
    vpc_id = var.vpc_id
    ingress {
        protocol = "TCP"
        from_port = 22
        to_port  = 22
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "TCP"
        from_port = "80"
        to_port  = "80"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        protocol = "-1"  
        from_port = 0
        to_port = 0
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "my-security-group"
        env = "dev"
    }
}

# variables

variable "image_id" {
    default = "ami-089c89a80285075f7"
}

variable "machine_type" {
    default = "t2.micro"
}

variable "vpc_id" {
    default = "vpc-0246017f2a6736ad6"
}


