### TERRAFORM

## what is terraform ?
* Terraform is an open-source infrastructure as code (IaC) software tool created by HashiCorp.
* It allows users to define and provision infrastructure resources such as virtual machines, networks, storage, and other components using a declarative configuration language. 
* Terraform configurations are written using HashiCorp Configuration Language (HCL) or JSON, and they describe the desired state of the infrastructure.

* Infrastructure as Code (IaC): is a method of managing and provisioning IT infrastructure using code, rather than manual configuration. 
* It allows teams to automate the setup and management of their infrastructure, making it more efficient and consistent. * This is particularly useful in the DevOps environment, where teams are constantly updating and deploying software.

## Why Terraform?

* Manage any infrastructure : Terraform takes an immutable approach to infrastructure, reducing the complexity of upgrading or modifying your services and infrastructure.


* Automate changes : Terraform configuration files are declarative, meaning that they describe the end state of your infrastructure. You do not need to write step-by-step instructions to create resources because Terraform handles the underlying logic. Terraform builds a resource graph to determine resource dependencies and creates or modifies non-dependent resources in parallel. This allows Terraform to provision resources efficiently.


* State Management : Terraform keeps track of the state of your infrastructure deployments in a state file, which helps it to understand which resources were created, updated, or deleted. This state file can be stored remotely, enabling collaboration and facilitating concurrent changes from multiple users.


* Plan and Preview Changes : Terraform provides a plan command that allows you to preview changes before applying them. This helps you understand the impact of your changes and detect any potential issues or conflicts in advance, reducing the risk of unexpected changes in production environments.


* Declarative Configuration : Terraform allows you to define your infrastructure in a declarative configuration language (HCL - HashiCorp Configuration Language), which means you specify the desired state of your infrastructure rather than writing procedural scripts to achieve it. This makes it easier to understand and manage infrastructure changes.

## Terraform Language
Terraform primarily uses its own configuration language called HashiCorp Configuration Language (HCL). However, it also supports JSON format for configurations. Here's a brief overview of each :

                     HashiCorp Configuration Language (HCL)

* HCL is designed specifically for authoring HashiCorp tools configurations, including Terraform.


* It's a declarative language, meaning you define the desired state of your infrastructure rather than writing procedural code.


* HCL is human-readable and easy to write, with a concise syntax that emphasizes readability and simplicity.


* HCL supports features like variables, expressions, functions, blocks, and modules, providing flexibility and modularity in your configurations.


                       JSON (JavaScript Object Notation) :


* Terraform also supports JSON format for configurations, in addition to HCL.


* JSON is a lightweight data interchange format widely used in web development.


* JSON configurations follow a strict syntax defined by the JSON specification.


* While JSON is less human-readable compared to HCL, it's machine-readable and can be useful for interoperability with other systems or for automating configuration generation.


## Terraform Language
.tf / .tf.json -- (extension)
 HCL / JSON -- (language)

 ## Terraform Blocks

* provider
* terraform
* resource
* data
* output
* variable
* module


## Terraform lifecycle

* init
* plan
* apply
* destroy


## Dependencies
* Implicit :
  * Create When one resource refers to another refers to anonther in its configuration.
  * Terraform automatically detects and manages these dependencies.
  * For example, if a resource depends on the output of another resource, Terrraform will establish an implicit    dependency.


* Explicit :
  * Defined using the ( depends_on ) argument in a resource block.
  * Allow you to explicity specify dependencies between resources.
  * useful in cases where imlicit dependencies might not be sufficient or clear.

